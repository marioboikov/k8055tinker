/*
 * Copyright (C) 2011 by Mario Boikov <mario@beblue.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef BOARD_H
#define BOARD_H

#include <QObject>
#include <QDeclarativeListProperty>

#include "ioport.h"
#include "k8055interface.h"

class Board : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int address READ address WRITE setAddress NOTIFY addressChanged)
    Q_PROPERTY(bool opened READ opened WRITE setOpened NOTIFY openedChanged)

    Q_PROPERTY(QDeclarativeListProperty<IOPort> ioports READ ioports)

    Q_PROPERTY(QString errorMsg READ errorMessage NOTIFY errorChanged)

    Q_CLASSINFO("DefaultProperty", "ioports")

    Q_ENUMS(BoardAddress)

public:
    enum BoardAddress {
        Board0 = 0,
        Board1 = 1,
        Board2 = 2,
        Board3 = 3,

        MinBoardAddress = Board0,
        MaxBoardAddress = Board3
    };

    explicit Board(QObject *parent = 0);

    bool opened() const;
    int address() const;

    QString errorMessage() const;

    QDeclarativeListProperty<IOPort> ioports();

signals:
    void addressChanged();
    void openedChanged();
    void errorChanged();

public slots:
    void setAddress(int address);
    void setOpened(bool opened);

    void k8055StatusChanged(K8055Interface::Status status);

private:

    Q_INVOKABLE void doOpenClose(bool open);

    K8055Interface m_k8055;

    int m_address;
    QString m_errorMsg;

    friend void boardAppendIOPort(QDeclarativeListProperty<IOPort> *property,
                                  IOPort *ioport);

};

#endif // BOARD_H
