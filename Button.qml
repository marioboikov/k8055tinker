import QtQuick 1.0

Rectangle {
    id: button

    signal clicked

    width: message.width + 20
    height: message.height + 10

    border { width: 1; color: "white" }

    radius: 4

    color: "transparent"

    Text {
        id: message

        anchors.centerIn: parent

        text: "Click me"
        font.pixelSize: 30
        color: "white"
    }

    MouseArea {
        id: mouseArea

        anchors.fill: parent
        onClicked: parent.clicked()
    }

    onClicked: SequentialAnimation {
        NumberAnimation { target: button; property: "scale"; to: 1.2; duration: 70 }
        NumberAnimation { target: button; property: "scale"; to: 1; duration: 100 }
    }
}
