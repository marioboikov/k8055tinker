import QtQuick 1.0
import k8055 1.0

Board {

    address: 0
    opened: true

    onErrorChanged: console.log("Error!")

    AnalogOutput {
        channel: AnalogOutput.Channel1

        SpringAnimation on value {
            from: 0
            to: 255

            // I'll just pick some "random" numbers for the spring props :)
            damping: 0
            spring: 0.1
            mass: 0.5

            running: true
        }
    }
}
