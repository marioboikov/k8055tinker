/*
 * Copyright (C) 2011 by Mario Boikov <mario@beblue.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "analoginput.h"
#include "k8055interface.h"

AnalogInput::AnalogInput(QObject *parent) :
    Input(parent)
{
    setValue(MinValue);

}

bool AnalogInput::validChannel(int channel) const
{
    return MinChannel <= channel && channel <= MaxChannel;
}

void AnalogInput::k8055InterfaceUpdated()
{
    if (m_k8055) {
        connect(m_k8055, SIGNAL(analogInputChanged(int, int)),
                SLOT(analogInputChanged(int,int)));
    }
}

void AnalogInput::analogInputChanged(int channel1, int channel2)
{
    int value = channel() == channel1 ? channel1 : channel2;
    setValue(value);
}



