/*
 * Copyright (C) 2011 by Mario Boikov <mario@beblue.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef OUTPUT_H
#define OUTPUT_H

#include <QObject>
#include <QPointer>

#include "ioport.h"
#include "k8055interface.h"

/**
 * Abstract base class for K8055 digital/analog outputs
 */
class Output : public IOPort
{
    Q_OBJECT

    Q_PROPERTY(int value READ value WRITE setValue NOTIFY valueChanged)

    Q_ENUMS(ValueRange)

public:
    explicit Output(QObject *parent = 0);

    int value() const;

    void setK8055Interface(K8055Interface *k8055);

signals:
    void valueChanged();

public slots:
    void setValue(int value);

protected:
    virtual bool validValue(int value) const = 0;
    virtual bool sendValue(K8055Interface *k8055, int channel, int value) = 0;

private slots:
    void k8055StatusChanged(K8055Interface::Status status);
    void submitValue();

private:
    int m_value;
    int m_valueToSend;

    QPointer<K8055Interface> m_k8055;
};

#endif // OUTPUT_H
