import QtQuick 1.0
import k8055 1.0

Board {

    address: 0
    opened: true

    onErrorChanged: console.log("Error!")

    DigitalInput {
        id: digIn
        channel: DigitalInput.Channel1
    }

    DigitalOutput {
        id: digOut
        channel: DigitalOutput.Channel8
        value: digIn.value // Property binding!
    }
}
