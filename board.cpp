/*
 * Copyright (C) 2011 by Mario Boikov <mario@beblue.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "board.h"
#include "ioport.h"

Board::Board(QObject *parent) :
    QObject(parent), m_address(-1)
{
    connect(&m_k8055, SIGNAL(statusChanged(K8055Interface::Status)),
            SLOT(k8055StatusChanged(K8055Interface::Status)));
}

bool Board::opened() const
{
    return m_k8055.status() == K8055Interface::Opened;
}

void Board::setOpened(bool opened)
{
    if (this->opened())
        return;

    QMetaObject::invokeMethod(this, "doOpenClose", Qt::QueuedConnection,
                              Q_ARG(bool, opened));
}

int Board::address() const
{
    return m_address;
}

void Board::setAddress(int address)
{
    if (m_address == address)
        return;

    m_address = address;
    emit addressChanged();
}

QString Board::errorMessage() const
{
    return m_errorMsg;
}

void Board::doOpenClose(bool open)
{
    if (m_address < MinBoardAddress || m_address > MaxBoardAddress)
        return;

    if (open) {
        m_k8055.openDevice(m_address);
    } else {
        m_k8055.closeDevice();
    }
}

void boardAppendIOPort(QDeclarativeListProperty<IOPort> *property,
                       IOPort *ioport)
{
    Board *board = static_cast<Board *>(property->data);

    if (board == 0)
        return;

    ioport->setK8055Interface(&board->m_k8055);
}

QDeclarativeListProperty<IOPort> Board::ioports()
{
    return QDeclarativeListProperty<IOPort>(this, this,
                                            ::boardAppendIOPort);
}

void Board::k8055StatusChanged(K8055Interface::Status status)
{
    if (status == K8055Interface::Opened) {
        emit openedChanged();
    } else if (status == K8055Interface::Closed) {
        emit openedChanged();
    } else if (status == K8055Interface::Error){
        m_errorMsg = QLatin1String("Error while communicating with the board");
        emit errorChanged();
    }
}
