/*
 * Copyright (C) 2011 by Mario Boikov <mario@beblue.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef DIGITALINPUT_H
#define DIGITALINPUT_H

#include "input.h"

class DigitalInput : public Input
{
    Q_OBJECT

    Q_ENUMS(Value)
    Q_ENUMS(Channel)

public:
    enum Value {
        Off = 0,
        On = 1,

        MinValue = Off,
        MaxValue = On
    };

    enum Channel {
        Channel1 = 1,
        Channel2 = 2,
        Channel3 = 3,
        Channel4 = 4,
        Channel5 = 5,

        MinChannel = Channel1,
        MaxChannel = Channel5
    };

    explicit DigitalInput(QObject *parent = 0);

protected:
    bool validChannel(int channel) const;
    void k8055InterfaceUpdated();

private slots:
    void digitalInputChanged(int value);
};

#endif // DIGITALINPUT_H
