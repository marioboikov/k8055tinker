TEMPLATE=app

QT += declarative

SOURCES += \
    main.cpp \
    libk8055.c \
    board.cpp \
    analogoutput.cpp \
    digitaloutput.cpp \
    k8055interface.cpp \
    output.cpp \
    ioport.cpp \
    input.cpp \
    digitalinput.cpp \
    analoginput.cpp

HEADERS += \
    k8055.h \
    board.h \
    analogoutput.h \
    digitaloutput.h \
    k8055interface.h \
    output.h \
    ioport.h \
    input.h \
    digitalinput.h \
    analoginput.h

LIBS += -lusb

OTHER_FILES += \
    Button.qml \
    example1.qml \
    example2.qml \
    example3.qml

RESOURCES += \
    qml.qrc
