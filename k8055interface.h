/*
 * Copyright (C) 2011 by Mario Boikov <mario@beblue.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef K8055INTERFACE_H
#define K8055INTERFACE_H

#include <QObject>
#include <QTimer>

class K8055Interface : public QObject
{
    Q_OBJECT

    Q_PROPERTY(Status status READ status NOTIFY statusChanged)

    Q_ENUMS(Status)

public:
    enum Status { Opened, Closed, Error };

    K8055Interface(QObject *parent = 0);

    Status status() const;

signals:
    void statusChanged(K8055Interface::Status status);

    void digitalInputChanged(int value);

    void analogInputChanged(int channel1, int channel2);

public slots:
    void openDevice(int address);

    void closeDevice();

    bool setAnalogChannel(int channel, int value);

    bool setDigitalChannel(int channel, int value);

private slots:
    void updateInput();

private:
    QTimer pollTimer;

    Status m_status;

    long m_digitalIn;
    long m_analogIn1;
    long m_analogIn2;
    long m_counter1;
    long m_counter2;

};

#endif // K8055INTERFACE_H
