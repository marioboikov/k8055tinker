/*
 * Copyright (C) 2011 by Mario Boikov <mario@beblue.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "input.h"
#include "k8055interface.h"

Input::Input(QObject *parent) :
    IOPort(parent), m_k8055(0), m_value(-1)
{
}

void Input::setK8055Interface(K8055Interface *k8055)
{
    if (m_k8055) {
        m_k8055->disconnect(this);
    }

    m_k8055 = k8055;
    k8055InterfaceUpdated();
}

int Input::value() const
{
    return m_value;
}

void Input::setValue(int value)
{
    if (m_value != value) {
        m_value = value;
        emit valueChanged();
    }
}
