import QtQuick 1.0
import k8055 1.0

Board {

    address: 0
    opened: true

    onErrorChanged: console.log("Error!")

    // A 'toogle' button
    DigitalInput {
        id: button

        property bool on: false

        channel: DigitalInput.Channel1

        onValueChanged: on = value ? !on: on
    }

    AnalogOutput {
        channel: AnalogOutput.Channel1

        value: button.on ? 255 : 0

        // Hey! Let's add a behavior
        Behavior on value {
            NumberAnimation { duration: 500; easing.type: Easing.InSine }
        }
    }
}
