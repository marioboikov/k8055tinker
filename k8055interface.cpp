/*
 * Copyright (C) 2011 by Mario Boikov <mario@beblue.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "k8055interface.h"
#include "k8055.h"

K8055Interface::K8055Interface(QObject *parent) :
    QObject(parent), m_status(Closed), m_digitalIn(0),
    m_analogIn1(0), m_analogIn2(0), m_counter1(0), m_counter2(0)
{
    connect(&pollTimer, SIGNAL(timeout()), SLOT(updateInput()));
}

void K8055Interface::openDevice(int address)
{
    if (m_status == Opened)
        return;

    if (OpenDevice(address) == address){
        m_status = Opened;
        pollTimer.start(50);
        emit statusChanged(Opened);
    } else {
        m_status = Error;
        emit statusChanged(Error);
    }
}

void K8055Interface::closeDevice()
{
    if (m_status == Opened) {
        pollTimer.stop();
        CloseDevice();
        m_status = Closed;
        emit statusChanged(Closed);
    }
}

bool K8055Interface::setAnalogChannel(int channel, int value)
{
    if (m_status != Opened)
        return false;

    return OutputAnalogChannel(channel, value) == 0;
}

K8055Interface::Status K8055Interface::status() const
{
    return m_status;
}

bool K8055Interface::setDigitalChannel(int channel, int value)
{
    if (m_status != Opened)
        return false;

    if (value) {
        return SetDigitalChannel(channel) == 0;
    } else {
        return ClearDigitalChannel(channel) == 0;
    }
}

void K8055Interface::updateInput()
{
    if (m_status != Opened)
        return;

    long prevDigIn = m_digitalIn;
    long prevAnalogIn1 = m_analogIn1;
    long prevAnalogIn2 = m_analogIn2;

    ReadAllValues(&m_digitalIn,
                  &m_analogIn1, &m_analogIn2,
                  &m_counter1, &m_counter2);

    if (m_digitalIn != prevDigIn) {
        emit digitalInputChanged(m_digitalIn);
    }

    if (m_analogIn1 != prevAnalogIn1 || m_analogIn2 != prevAnalogIn2) {
        emit analogInputChanged(m_analogIn1, m_analogIn2);
    }
}
