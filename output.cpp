/*
 * Copyright (C) 2011 by Mario Boikov <mario@beblue.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "output.h"

Output::Output(QObject *parent) :
    IOPort(parent), m_value(-1), m_valueToSend(-1), m_k8055(0)
{
    connect(this, SIGNAL(channelChanged()),
            SLOT(submitValue()), Qt::QueuedConnection);
}

int Output::value() const
{
    return m_value;
}

void Output::setValue(int value)
{
    if (validValue(value) && value != m_valueToSend) {
        m_valueToSend = value;
        QMetaObject::invokeMethod(this, "submitValue", Qt::QueuedConnection);
    }
}

void Output::submitValue()
{
    if (validChannel(m_channel) && validValue(m_valueToSend) &&
            m_k8055 && sendValue(m_k8055, m_channel, m_valueToSend)) {

        m_value = m_valueToSend;
        emit valueChanged();
    }
}

void Output::setK8055Interface(K8055Interface *k8055)
{
    if (m_k8055) {
        disconnect(m_k8055, SIGNAL(statusChanged(K8055Interface::Status)));
    }

    m_k8055 = k8055;

    if (m_k8055) {
        connect(m_k8055, SIGNAL(statusChanged(K8055Interface::Status)),
                SLOT(k8055StatusChanged(K8055Interface::Status)));
        k8055StatusChanged(m_k8055->status());
    }
}

void Output::k8055StatusChanged(K8055Interface::Status status)
{
    if (status == K8055Interface::Opened) {
        QMetaObject::invokeMethod(this, "submitValue", Qt::QueuedConnection);
    }
}
