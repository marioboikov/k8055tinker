/*
 * Copyright (C) 2011 by Mario Boikov <mario@beblue.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <QApplication>
#include <QtDeclarative>
#include <QPushButton>

#include "board.h"
#include "analogoutput.h"
#include "digitaloutput.h"
#include "analoginput.h"
#include "digitalinput.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    qmlRegisterType<Board>("k8055", 1, 0, "Board");
    qmlRegisterUncreatableType<IOPort>("k8055", 1, 0, "IOPort", "Abstract base class");
    qmlRegisterUncreatableType<Input>("k8055", 1, 0, "Input", "Abstract base class");
    qmlRegisterUncreatableType<Output>("k8055", 1, 0, "Output", "Abstract base class");
    qmlRegisterType<AnalogOutput>("k8055", 1, 0, "AnalogOutput");
    qmlRegisterType<AnalogInput>("k8055", 1, 0, "AnalogInput");
    qmlRegisterType<DigitalOutput>("k8055", 1, 0, "DigitalOutput");
    qmlRegisterType<DigitalInput>("k8055", 1, 0, "DigitalInput");

    QDeclarativeEngine *engine = new QDeclarativeEngine;
    QDeclarativeComponent component(engine, QUrl("qrc:/example3.qml"));

    Board *board = qobject_cast<Board *>(component.create());

    if (board) {
        qDebug("Yes, it's a board!");
    }

    QPushButton button("Quit");
    button.resize(100, 50);
    button.setVisible(true);
    app.connect(&button, SIGNAL(clicked()), SLOT(quit()));

    return app.exec();
}
